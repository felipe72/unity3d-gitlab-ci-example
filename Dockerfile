from ubuntu:16.04

RUN apt-get update && apt-get install unzip wget zip -y 
RUN wget https://broth.itch.ovh/butler/linux-amd64/LATEST/archive/default && unzip default && mv butler /bin/ && chmod +x /bin/butler
