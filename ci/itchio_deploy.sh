#!/bin/env bash

apk add --no-cache --update bash unzip wget
wget https://broth.itch.ovh/butler/linux-amd64/LATEST/archive/default && unzip default && mv butler /bin/
butler push Builds/StandaloneLinux64/Grimheart.deb pixel-noon/grimheart:linux-64
