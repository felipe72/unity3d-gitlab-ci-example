#!/bin/bash
#
# Generates .deb package for Linux
#

# Include project metadata
. metadata.ini
PACKAGE_NAME=$PACKAGE_NAME
EXECUTABLE_NAME=$EXECUTABLE_NAME
PACKAGE_VERSION=$VERSION_MAJOR.$VERSION_MINOR-$VERSION_RELEASE
OUTPUT_FILE=$PACKAGE_NAME\_$PACKAGE_VERSION.deb

function gen_deb()
{
    # Build dir
    tmp_dir=/tmp/$PACKAGE_NAME\_$PACKAGE_VERSION
    rm -rf $tmp_dir
    mkdir -p $tmp_dir

    # Data dir: resources, scripts and executable
    var_dir=$tmp_dir/usr
    exec_dir=$var_dir/bin
    data_dir=$var_dir/local/games
    install_dir=$data_dir
    mkdir -p $install_dir
    mkdir -p $exec_dir/$PACKAGE_NAME
    fakeroot chmod 755 run.sh
    fakeroot cp run.sh $exec_dir/$PACKAGE_NAME
    fakeroot cp -r ../../Builds/$PACKAGE_NAME $install_dir
    # Debian package info dir
    mkdir -p $tmp_dir/DEBIAN
    fakeroot cp control $tmp_dir/DEBIAN/
    sed -i 's/%PACKAGE%/'"$PACKAGE_NAME"'/g' $tmp_dir/DEBIAN/control
    sed -i 's/%VERSION%/'"$PACKAGE_VERSION"'/g' $tmp_dir/DEBIAN/control
    sed -i 's/%ARCHITECTURE%/'"$ARCHITECTURE"'/g' $tmp_dir/DEBIAN/control
    sed -i 's/%MAINTAINER%/'"$MAINTAINER"'/g' $tmp_dir/DEBIAN/control
    sed -i 's/%MAINTAINER_CONTACT%/'"$MAINTAINER_CONTACT"'/g' $tmp_dir/DEBIAN/control
    sed -i 's/%DESCRIPTION%/'"$DESCRIPTION"'/g' $tmp_dir/DEBIAN/control

    # Set the permissions
    fakeroot chmod 755 $install_dir/$PACKAGE_NAME/$EXECUTABLE_NAME
    fakeroot chmod 755 $exec_dir/$PACKAGE_NAME
    # Build and check the package
    fakeroot dpkg-deb --build $tmp_dir
    mv /tmp/$OUTPUT_FILE $EXECUTABLE_NAME.deb
}

echo "Generating "$EXECUTABLE_NAME".deb ..."
gen_deb
echo "Done"
